# tc-tesla.com sources

Server-side working on top [Express.js](http://expressjs.com), with [Mongoose.js](http://mongoosejs.com) as ORM and
[Ractive.js](http://www.ractivejs.org) as view engine.

Front-end is built with Less.js, Browserify and Gulp.

Some (huge) amount of CSS-code borrowed from [Bootstrap](http://getbootstrap.com).

Many thanks to [Davi Ferreira](https://github.com/daviferreira) for [Medium editor](https://github.com/daviferreira/medium-editor).

Special thanks for [Josip Kelava](http://josipkelava.com/) for awesome [Metropolis 1920](http://incredibletypes.com/metropolis-1920/) font.

## Runinin in dev mode

Make sure you have mongodb (2.6 or higher) installed.

You'll need node 0.11 or higher and [Gulp](https://www.npmjs.com/packages/gulp) installed globally.

1. Clone it.
2. CD into it.
3. Run `gulp bower && gulp dev`.
4. Open [http://localhost:3000/](http://localhost:3000/) in your browser.

